let express = require('express');
let app = express();

let nodeadmin = require('nodeadmin');
app.use(nodeadmin(app));

app.listen(process.env.APP_PORT);