build: ## Command to build image using Dockerfile
	docker build -t biletskiydm/nodeadmin:1.0.0 .
run: ## Runs server
	docker run --name nodeadmin -d -it -p 7878:7878 biletskiydm/nodeadmin
bash: ## Connects to container's cli
	docker exec --it nodeadmin /bin/bash
