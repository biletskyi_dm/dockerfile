# Uses node-alpine image as base
FROM node:10.15.3-alpine

# Sets /app as root app dir
WORKDIR /app

# Copies app file and npm config file
COPY app.js ./
COPY package.json ./

# Installs required software to expand Alpine and
# creates empty package.json file and installs nodeadmin as dependency
RUN apk add --no-cache \
    bash \
    bind-tools \
    curl \
    git \
    lsof \
    mysql-client \
    nano \
    openssh-client \
    vim \
    && npm i

# Uses port as default
ENV APP_PORT 7878
EXPOSE 7878

# Runs script
CMD ["node", "app.js"]
